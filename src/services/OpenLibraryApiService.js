import axios from "axios";
import EventBus from "./EventBus.js";

const OpenLibraryApiService = {
  async searchForTitle(title) {
    const foundData = await axios.get(
      "https://openlibrary.org/search.json?title=" + title.replaceAll(" ", "+")
    );

    EventBus.emit("books.update", foundData.data.docs);
  },
  getCoverUrlFor(book, size = "M") {
    return `https://covers.openlibrary.org/b/id/${book.cover_i}-${size}.jpg`;
  },
};

EventBus.on("query.update", OpenLibraryApiService.searchForTitle);

export default OpenLibraryApiService;
