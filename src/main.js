import Vue from "vue";
import LayoutComponent from "./LayoutComponent.vue";

// Load up the API service
import "./services/OpenLibraryApiService.js";

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(LayoutComponent),
}).$mount("#app");
