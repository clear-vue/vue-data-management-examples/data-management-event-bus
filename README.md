# Data Management: Event Bus

This application illustrates the book search application as an event bus
managed application. All data management is passed to different components using
events on a shared event bus.

Any change that a component requests of the data  is emitted through an event
and then handled by a central service that is listening
for those events. New data is then communicated back to the components through
events.

This illustrates some key details of this kind of application structure:

1. UI interaction kicks off events that other parts of the system can listen for, if they can handle those events.
2. While components are nested, there's no real concept of a 'parent' or 'child' component.
3. Components don't really know that other components exist. It's only by listening or emitting events that interactions between the components happen.
4. Some data is still managed by 'props' when a component has multiples of another component, like 'BookTiles' having many 'BookTile' components.

This can work on applications where components are very loosely coupled and can be quickly swapped out for other components. The big documentation task here is documenting all the different events and what they mean and contain as data. Data duplication in memory will be the biggest issue with an application like this.